//
// Created by dnatov on 9/27/2021.
//

#include <math.h>
#include "mcp41XXX.h"

void MCP41_Digipot_SetResistance(struct MCP41_Digipot* instance, uint32_t resistance)
{
    float resistanceSteps = instance->MaxResistance/255;
    uint8_t byteValue = (uint8_t)round(resistance/resistanceSteps);
    uint8_t commandByte = 0b00010001; // Write to Potentionmeter 0
    uint8_t byteArray[2] = { byteValue, commandByte };
    instance->SPI_ReadWriteMethodPtr(byteArray, 2, instance->ChipSelectID, spmMode0);
}