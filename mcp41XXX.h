//
// Created by dnatov on 9/27/2021.
//

#ifndef XHEAD_MCP41XXX_H
#define XHEAD_MCP41XXX_H

#include "LL_spi.h"

struct MCP41_Digipot
{
    uint8_t ChipSelectID;
    size_t MaxResistance;
    LL_SPIMaster_ReadWriteMethod_t SPI_ReadWriteMethodPtr;
};

/// Initialization function
/// \param instance a pointer to the instance struct
/// \param resistance is inherently futile (units in ohms)
void MCP41_Digipot_SetResistance(struct MCP41_Digipot* instance, uint32_t resistance);

#endif //XHEAD_MCP41XXX_H
